<?php


namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\ShipBuilder\Director;
use BinaryStudioAcademy\Game\ShipBuilder\RoyalPatroolSchoonerBuilder;

class SaltEndHarbor
{
    private $name;
    private $ship;
    private $allowedDirections;
    private $isEnemyShip;

    public function __construct()
    {
        $this->name = "Salt End";
        $this->allowedDirections = array('west' => 3, 'east' => 5, 'south' => 1);
        $shipBuilder = new RoyalPatroolSchoonerBuilder();
        $ship = (new Director())->build($shipBuilder);
        $this->ship = $ship;
        $this->isEnemyShip = true;
    }

    public function getAllowedDirections():array
    {
        return $this->allowedDirections;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getShip()
    {
        return $this->ship;
    }
}