<?php


namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\ShipBuilder\Director;
use BinaryStudioAcademy\Game\ShipBuilder\RoyalPatroolSchoonerBuilder;

class FishguardHarbor
{
    private $name;
    private $ship;
    private $allowedDirections;
    private $isEnemyShip;

    public function __construct()
    {
        $this->name = "Fishguard";
        $this->allowedDirections = array('north' => 4, 'east' => 1, 'south' => 2);
        $shipBuilder = new RoyalPatroolSchoonerBuilder();
        $ship = (new Director())->build($shipBuilder);
        $this->ship = $ship;
        $this->isEnemyShip = true;
    }

    public function getAllowedDirections():array
    {
        return $this->allowedDirections;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getShip()
    {
        return $this->ship;
    }
}