<?php


namespace BinaryStudioAcademy\Game\Harbors;


class PiratesHarbor
{
    private $name;
    //private $ship;
    private $allowedDirections;
    private $isEnemyShip;

    public function __construct()
    {
        $this->name = "Pirates Harbor";
        $this->allowedDirections = array('north' => 4, 'south' => 2, 'west' => 3);
        $this->isEnemyShip = false;
    }

    public function getAllowedDirections():array
    {
        return $this->allowedDirections;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getShip()
    {
        return $this->ship;
    }
}