<?php


namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\ShipBuilder\Director;
use BinaryStudioAcademy\Game\ShipBuilder\HMSRoyalSovereignBuilder;

class LondonDocksHarbor
{
    private $name;
    private $ship;
    private $allowedDirections;
    private $isEnemyShip;

    public function __construct()
    {
        $this->name = "London Docks";
        $this->allowedDirections = array('west' => 7, 'north' => 6);
        $shipBuilder = new HMSRoyalSovereignBuilder();
        $ship = (new Director())->build($shipBuilder);
        $this->ship = $ship;
        $this->isEnemyShip = true;
    }

    public function getAllowedDirections():array
    {
        return $this->allowedDirections;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getShip()
    {
        return $this->ship;
    }
}