<?php


namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\ShipBuilder\Director;
use BinaryStudioAcademy\Game\ShipBuilder\RoyalPatroolSchoonerBuilder;

class SouthhamptonHarbor
{
    private $name;
    private $ship;
    private $allowedDirections;
    private $isEnemyShip;

    public function __construct()
    {
        $this->name = "Southhampton";
        $this->allowedDirections = array('north' => 1, 'east' => 7, 'west' => 3);
        $shipBuilder = new RoyalPatroolSchoonerBuilder();
        $ship = (new Director())->build($shipBuilder);
        $this->ship = $ship;
        $this->isEnemyShip = true;
    }

    public function getAllowedDirections():array
    {
        return $this->allowedDirections;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getShip()
    {
        return $this->ship;
    }
}