<?php


namespace BinaryStudioAcademy\Game\Harbors;


class HarborFactory
{
    public function createPirayesHarbor(): PiratesHarbor
    {
        return new PiratesHarbor();
    }

    public function createFelixstoweHarbor(): FelixstoweHarbor
    {
        return new FelixstoweHarbor();
    }

    public function createFishguardHarbor(): FishguardHarbor
    {
        return new FishguardHarbor();
    }

    public function createGraysHarbor(): GraysHarbor
    {
        return new GraysHarbor();
    }

    public function createIsleOfGrainHarbor(): IsleOfGrainHarbor
    {
        return new IsleOfGrainHarbor();
    }

    public function createLondonDocksHarbor(): LondonDocksHarbor
    {
        return new LondonDocksHarbor();
    }

    public function createSaltEndHarbor(): SaltEndHarbor
    {
        return new SaltEndHarbor();
    }

    public function createSouthhamptonHarbor(): SouthhamptonHarbor
    {
        return new SouthhamptonHarbor();
    }
}