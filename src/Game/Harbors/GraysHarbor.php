<?php


namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\ShipBuilder\Director;
use BinaryStudioAcademy\Game\ShipBuilder\RoyalBattleShipBuilder;

class GraysHarbor
{
    private $name;
    private $ship;
    private $allowedDirections;
    private $isEnemyShip;

    public function __construct()
    {
        $this->name = "Grays";
        $this->allowedDirections = array('west' => 5, 'south' => 8);
        $shipBuilder = new RoyalBattleShipBuilder();
        $ship = (new Director())->build($shipBuilder);
        $this->ship = $ship;
        $this->isEnemyShip = true;
    }

    public function getAllowedDirections():array
    {
        return $this->allowedDirections;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getShip()
    {
        return $this->ship;
    }
}