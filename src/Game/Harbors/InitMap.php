<?php


namespace BinaryStudioAcademy\Game\Harbors;


class InitMap
{
    private $map = [];

    public function __construct()
    {
        $factory = new HarborFactory();
        $this->map = array(
            1 => $factory->createPirayesHarbor(),
            2 => $factory->createSouthhamptonHarbor(),
            3 => $factory->createFishguardHarbor(),
            4 => $factory->createSaltEndHarbor(),
            5 => $factory->createIsleOfGrainHarbor(),
            6 => $factory->createGraysHarbor(),
            7 => $factory->createFelixstoweHarbor(),
            8 => $factory->createLondonDocksHarbor()
        );
    }

    public function getMap()
    {
        return $this->map;
    }
}