<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\ShipBuilder\Director;
use BinaryStudioAcademy\Game\ShipBuilder\PiratesShipBuilder;
use BinaryStudioAcademyTests\Game\Messages;

class Game
{
    private $random;
    private $map;
    private $cmd;
    private $ship;

    public function __construct(Random $random)
    {
        $this->random = $random;
        $this->map = (new \BinaryStudioAcademy\Game\Harbors\InitMap())->getMap();
        $this->cmd = new \BinaryStudioAcademy\Game\Commands\Command();
        $shipBuilder = new PiratesShipBuilder();
        $this->ship = (new Director())->build($shipBuilder);
    }

    public function start(Reader $reader, Writer $writer)
    {
        /*$writer->writeln('Your task is to develop a game "Battle EnemyShip".');
        $writer->writeln('This method starts infinite loop with game logic.');
        $writer->writeln('Use proposed implementation in order to tests work correct.');
        $writer->writeln('Random float number: ' . $this->random->get());
        $writer->writeln('Feel free to remove this lines and write yours instead.');*/
        $writer->writeln('Press enter to start... ');
        $input = trim($reader->read());
        $writer->writeln('Adventure has begun. Wish you good luck!');
        while (true) {
            $this->run($reader, $writer);
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        //$writer->writeln('This method runs program step by step.');
        $input = trim($reader->read());
        $command = $this->cmd->cmdReceiver($input);
        if ($command[0] === "help") {
            $writer->write($this->cmd->help());
        }
        if ($command[0] === "stats" ){
            $writer->write($this->cmd->stats($this->ship));
        }

        if ($command[0] === "exit") {
            exit;
        }

        if ($command[0] === "set-sail") {
            $writer->write($this->cmd->setSail($this->ship, $this->map, $command[1]));
        }

        if ($command[0] === "whereami") {
            $writer->write($this->cmd->whereAmI($this->ship));
        }
         if ($command[0] === "fire") {
             $currentLocation = $this->map[$this->ship->getWhereAmI()];
             $enemyShip = $currentLocation->getShip();
             $writer->write($this->cmd->fire($this->ship, $enemyShip, $this->random));
         }
    }
}
