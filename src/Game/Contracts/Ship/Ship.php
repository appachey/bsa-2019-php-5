<?php


namespace BinaryStudioAcademy\Game\Contracts\Ship;


interface Ship
{
    public function getName(): string;

    public function getStrength(): integer;

    public function getArmour(): integer;

    public function getLuck(): integer;

    public function getHealth(): integer;

    public function getHold(): array;

    public function getStats(): array;

    public function setName($value);

    public function setStrength($value);

    public function setArmour($value);

    public function setLuck($value);

    public function setHealth($value);

    public function setHold($value);
}