<?php


namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Harbors\InitMap;
use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\ShipStats;

class Command
{
    const HARBORS = [
        1 => [
            'harbor' => 'Pirates Harbor',
            'ship' => 'player'
        ],
        2 => [
            'harbor' => 'Southhampton',
            'ship' => 'schooner'
        ],
        3 => [
            'harbor' => 'Fishguard',
            'ship' => 'schooner'
        ],
        4 => [
            'harbor' => 'Salt End',
            'ship' => 'schooner'
        ],
        5 => [
            'harbor' => 'Isle of Grain',
            'ship' => 'schooner'
        ],
        6 => [
            'harbor' => 'Grays',
            'ship' => 'battle'
        ],
        7 => [
            'harbor' => 'Felixstowe',
            'ship' => 'battle'
        ],
        8 => [
            'harbor' => 'London Docks',
            'ship' => 'royal'
        ]
    ];

    public function setSail($ship, $map, $direction): string
    {
        $currentLocation = $map[$ship->getWhereAmI()];
        $avaliableDirections = $currentLocation->getAllowedDirections();
        if (array_key_exists($direction, $avaliableDirections)) {
            $ship->setWhereAmI($avaliableDirections[$direction]);
            return $this->harborInfo($ship, $map);
        } else {
            return $this->errors("incorrect_direction");
        }
    }

    public function harborInfo($ship, $map)
    {
        if ($ship->getWhereAmI() === 1) {
            if ($ship->getHealth() < 60) {
                $ship->repair();
                return 'Harbor 1: Pirates Harbor.' . PHP_EOL
                    . 'Your health is repared to 60.' . PHP_EOL;
            } else {
                return 'Harbor 1: Pirates Harbor.' . PHP_EOL;
            }
        }
        $newLocation = $map[$ship->getWhereAmI()];
        $enemyShip = $newLocation->getShip();
        $enemyShipStats = $this->shipGetStats($enemyShip);
        $enemyShipName = $enemyShip->getName();
        return "Harbor {$ship->getWhereAmI()}: {$newLocation->getName()}." . PHP_EOL
            . "You see {$enemyShipName}: " . PHP_EOL
            . 'strength: ' . $enemyShipStats['strength'] . PHP_EOL
            . 'armour: ' . $enemyShipStats['armour'] . PHP_EOL
            . 'luck: ' . $enemyShipStats['luck'] . PHP_EOL
            . 'health: ' . $enemyShipStats['health'] . PHP_EOL;
    }

    public function whereAmI($ship)
    {
        $whereAmI = $ship->getWhereAmI();
        $harbor = self::HARBORS[$whereAmI];
        return "Harbor {$whereAmI}: {$harbor['harbor']}" . PHP_EOL;
    }

    public function shipGetStats($ship): array
    {
        $hold = '';
        if (empty($ship->getHold())) {
            $hold .= "[ _ _ _ ]";
        } else {
            $count = count($ship->getHold());
            $hold .= "[ ";
            foreach ($ship->getHold() as $item) {
                $hold .= $item . " ";
            }
            if ($count < 3) {
                for ($i = 0; $i < 3 - $count; $i++) {
                    $hold .= "_ ";
                }
            }
            $hold .= "]";
        }
        return [
            'strength' => $ship->getStrength(),
            'armour' => $ship->getArmour(),
            'luck' => $ship->getLuck(),
            'health' => $ship->getHealth(),
            'hold' => $hold
        ];
    }

    public function fire($playerShip, $enemyShip, $random): string
    {
        $math = new Math();
        while ($enemyShip->getHealth() > 0 && $playerShip->getHealth() > 0) {
            if ($math->luck($random, $playerShip->getLuck())) {
                $enemyDamage = $math->damage($enemyShip->getStrength(), $enemyShip->getArmour());
                $myDamage = $math->damage($playerShip->getStrength(), $playerShip->getArmour());
                $enemyShip->setHealth($enemyShip->getHealth() - $enemyDamage);
                $playerShip->setHealth($playerShip->getHealth() - $myDamage);
                if ($enemyShip->getHealth() > 0 && $playerShip->getHealth() > 0) {
                    return "{$enemyShip->getName()} has damaged on: {$enemyDamage} points." . PHP_EOL
                        . "health: {$enemyShip->getHealth()}" . PHP_EOL
                        . "{$enemyShip->getName()} damaged your ship on: {$myDamage} points." . PHP_EOL
                        . "health: {$playerShip->getHealth()}" . PHP_EOL;
                }
                if ($enemyShip->getHealth() <= 0) {
                    return $this->win($enemyShip->getName());
                } else if ($playerShip->getHealth() <= 0) {
                    $playerShip->setWhereAmI(1);
                    return "Your ship has been sunk." . PHP_EOL
                        . "You restored in the Pirate Harbor." . PHP_EOL
                        . "You lost all your possessions and 1 of each stats." . PHP_EOL;
                }
            }
        }

    }

    public function win(string $shipName): string
    {
        if ($shipName === 'HMS Royal Sovereign') {
            return '🎉🎉🎉Congratulations🎉🎉🎉' . PHP_EOL
                . '💰💰💰 All gold and rum of Great Britain belong to you! 🍾🍾🍾';
        } else {
            return "{$shipName} on fire. Take it to the boarding." . PHP_EOL;
        }
    }

    public function cmdReceiver ($command): array
    {
        $cmdArgs = preg_split( "/[\s]+/", $command);
        return $cmdArgs;
    }

    public function errors(string $key): string
    {
        return [
            'incorrect_direction' => 'Harbor not found in this direction',
            'pirate_harbor_aboard' => 'There is no ship to aboard',
            'pirate_harbor_fire' => 'There is no ship to fight',
            'aboard_live_ship' => 'You cannot board this ship, since it has not yet sunk',
            'incorrect_direction_command' => "Direction 'asd' incorrect, choose from: east, west, north, south",
            'unknown_command' => "Command 'unknown_command' not found"
        ][$key];
    }

    public function help(): string
    {
        return 'List of commands:' . PHP_EOL
            . 'help - shows this list of commands' . PHP_EOL
            . 'stats - shows stats of ship' . PHP_EOL
            . 'set-sail <east|west|north|south> - moves in given direction' . PHP_EOL
            . 'fire - attacks enemy\'s ship' . PHP_EOL
            . 'aboard - collect loot from the ship' . PHP_EOL
            . 'buy <strength|armour|luck|rum> - buys skill or rum: 1 chest of gold - 1 item' . PHP_EOL
            . 'drink - your captain drinks 1 bottle of rum and fill 30 points of health' . PHP_EOL
            . 'whereami - shows current harbor' . PHP_EOL
            . 'exit - finishes the game' . PHP_EOL;
    }

    public function stats($ship): string
    {
        $stats = $this->shipGetStats($ship);
        return 'EnemyShip stats:' . PHP_EOL
            . 'strength: ' . ($stats['strength']) . PHP_EOL
            . 'armour: ' . ($stats['armour']) . PHP_EOL
            . 'luck: ' . ($stats['luck'])  . PHP_EOL
            . 'health: ' . ($stats['health'])   . PHP_EOL
            . 'hold: ' . ($stats['hold']) . PHP_EOL;
    }

    public function aboard($playerShip): string
    {
        //if ($playerShip-)
    }
}