<?php
namespace BinaryStudioAcademy\Game\ShipBuilder;

class RoyalPatroolSchoonerBuilder implements ShipBuilder
{
    private $ship;

    public function setNameValue()
    {
        $this->ship->setName("Royal Patrool Schooner");
    }

    public function setStrengthValue()
    {
        $this->ship->setStrength(4);
    }

    public function setArmourValue()
    {
        $this->ship->setArmour(4);
    }

    public function setLuckValue()
    {
        $this->ship->setLuck(4);
    }

    public function setHealthValue()
    {
        $this->ship->setHealth(50);
    }

    public function setHoldValue()
    {
        $this->ship->setHold(array("💰"));
    }

    public function getShip():EnemyShip
    {
        return $this->ship;
    }

    public function buildShip()
    {
        $this->ship = new EnemyShip();
    }
}