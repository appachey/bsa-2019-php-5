<?php
namespace BinaryStudioAcademy\Game\ShipBuilder;

use BinaryStudioAcademy\Game\Helpers\Stats;

class HMSRoyalSovereignBuilder implements ShipBuilder
{
    private $ship;

    public function setNameValue()
    {
        $this->ship->setName("HMS Royal Sovereign");
    }

    public function setStrengthValue()
    {
        $this->ship->setStrength(Stats::MAX_STRENGTH);
    }

    public function setArmourValue()
    {
        $this->ship->setArmour(Stats::MAX_ARMOUR);
    }

    public function setLuckValue()
    {
        $this->ship->setLuck(Stats::MAX_LUCK);
    }

    public function setHealthValue()
    {
        $this->ship->setHealth(Stats::MAX_HEALTH);
    }

    public function setHoldValue()
    {
        $this->ship->setHold(array("💰", "💰", "🍾"));
    }

    public function getShip():EnemyShip
    {
        return $this->ship;
    }

    public function buildShip()
    {
        $this->ship = new EnemyShip();
    }
}