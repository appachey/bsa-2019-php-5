<?php
namespace BinaryStudioAcademy\Game\ShipBuilder;

class PiratesShipBuilder implements ShipBuilder
{

    private $ship;

    public function setNameValue()
    {
        $this->ship->setName('Pirates Ship');
    }

    public function setStrengthValue()
    {
        $this->ship->setStrength(4);
    }

    public function setArmourValue()
    {
        $this->ship->setArmour(4);
    }

    public function setLuckValue()
    {
        $this->ship->setLuck(4);
    }

    public function setHealthValue()
    {
        $this->ship->setHealth(60);
    }

    public function setHoldValue()
    {
        $this->ship->setHold([]);
    }

    public function getShip():PiratesShip
    {
        return $this->ship;
    }

    public function buildShip()
    {
        $this->ship = new PiratesShip();
    }
}