<?php
namespace BinaryStudioAcademy\Game\ShipBuilder;

interface ShipBuilder
{
    public function setNameValue();

    public function setStrengthValue();

    public function setArmourValue();

    public function setLuckValue();

    public function setHealthValue();

    public function setHoldValue();

    public function getShip();

    public function buildShip();
}