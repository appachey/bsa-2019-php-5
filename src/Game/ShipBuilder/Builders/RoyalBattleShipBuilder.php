<?php
namespace BinaryStudioAcademy\Game\ShipBuilder;

class RoyalBattleShipBuilder implements ShipBuilder
{
    private $ship;

    public function setNameValue()
    {
        $this->ship->setName("Royal Battle EnemyShip");
    }

    public function setStrengthValue()
    {
        $this->ship->setStrength(8);
    }

    public function setArmourValue()
    {
        $this->ship->setArmour(8);
    }

    public function setLuckValue()
    {
        $this->ship->setLuck(7);
    }

    public function setHealthValue()
    {
        $this->ship->setHealth(80);
    }

    public function setHoldValue()
    {
        $this->ship->setHold(array("🍾"));
    }

    public function getShip():EnemyShip
    {
        return $this->ship;
    }

    public function buildShip()
    {
        $this->ship = new EnemyShip();
    }
}