<?php


namespace BinaryStudioAcademy\Game\ShipBuilder\Ships;

use BinaryStudioAcademy\Game\Contracts\Ship\Ship as IShip;

class Ship implements IShip
{
    private $name;
    private $strength;
    private $armour;
    private $luck;
    private $health;
    private $hold = [];

    public function getName(): string
    {
        return $this->name;
    }

    public function getStrength(): integer
    {
        return $this->strength;
    }

    public function getArmour(): integer
    {
        return $this->armour;
    }

    public function getLuck(): integer
    {
        return $this->luck;
    }

    public function getHealth(): integer
    {
        return $this->health;
    }

    public function getHold(): array
    {
        return $this->hold;
    }

    public function getStats(): array
    {
        $hold = '';
        if (empty($this->hold)) {
            $hold .= "[ _ _ _ ]";
        } else {
            $count = count($this->hold);
            $hold .= "[ ";
            foreach ($this->hold as $item) {
                $hold .= $item . " ";
            }
            if ($count < 3) {
                for ($i = 0; $i < 3 - $count; $i++) {
                    $hold .= "_ ";
                }
            }
            $hold .= "]";
        }
        return [
            'strength' => $this->strength,
            'armour' => $this->armour,
            'luck' => $this->luck,
            'health' => $this->health,
            'hold' => $hold
        ];
    }

    public function setName($value)
    {
        $this->name = $value;
    }

    public function setStrength($value)
    {
        $this->strength = $value;
    }

    public function setArmour($value)
    {
        $this->armour = $value;
    }

    public function setLuck($value)
    {
        $this->luck = $value;
    }

    public function setHealth($value)
    {
        $this->health = $value;
    }

    public function setHold($value)
    {
        $this->hold = $value;
    }
}