<?php
namespace BinaryStudioAcademy\Game\ShipBuilder;

use BinaryStudioAcademy\Game\ShipBuilder\Ships\Ship;

class PiratesShip extends Ship
{
    public function getWhereAmI(): int
    {
        return $this->whereAmI;
    }

    public function setWhereAmI($location)
    {
        $this->whereAmI = $location;
    }

    public function repair()
    {
        $this->setHealth(60);
    }
}