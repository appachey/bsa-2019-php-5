<?php
namespace BinaryStudioAcademy\Game\ShipBuilder;

class Director
{
    public function build(ShipBuilder $builder):EnemyShip
    {
        $builder->buildShip();
        $builder->setNameValue();
        $builder->setStrengthValue();
        $builder->setArmourValue();
        $builder->setLuckValue();
        $builder->setHealthValue();
        $builder->setHoldValue();
        return $builder->getShip();
    }
}